## Nectar 360 Dashboard by Romilly

## How to use

Install the Express server and run:

```sh
npm install
npm start
```

The server should be run on localhost port 5000.

Install the React client and run:

```sh
npm install
npm start
```
