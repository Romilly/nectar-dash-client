import React, { useState } from 'react';
import { ResponsiveLine } from '@nivo/line';
import { linearGradientDef } from '@nivo/core';
import Title from './Title';
import axios from 'axios';
import Spinner from './Spinner';

// note from Nivo:
// make sure parent container has a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.

const MyResponsiveLine = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [chartData, setChartData] = useState([]);

  // Define empty object to fill values from API
  const dataFromMysql = {};

  // Get weekly Control Data
  const getControlData = async () => {
    try {
      const res = await axios.get(`/getweekly/control/${props.category}`);

      // Change field names of returned array so data can be plotted on chart (WEEK_COMMENCING to x and CONTROL to y)
      const control = res.data.map(({ WEEK_COMMENCING, CONTROL }) => ({
        x: WEEK_COMMENCING,
        y: CONTROL,
      }));

      // add result to dataFromMysql object
      dataFromMysql.control = control;
    } catch (err) {
      console.log(err);
      setIsError(true);
      setIsLoading(false);
    }
  };

  // Get weekly Exposed Data
  const getExposedData = async () => {
    try {
      const res = await axios.get(`/getweekly/exposed/${props.category}`);

      // Change field names of returned array so data can be plotted on chart (WEEK_COMMENCING to x and CONTROL to y)
      const exposed = res.data.map(({ WEEK_COMMENCING, EXPOSED }) => ({
        x: WEEK_COMMENCING,
        y: EXPOSED,
      }));

      // add result to dataFromMysql object
      dataFromMysql.exposed = exposed;
    } catch (err) {
      console.log(err);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading) {
    getControlData()
      .then(() => getExposedData())
      .then(() => {
        setIsLoading(false);
        setChartData([
          {
            id: 'Control',
            data: dataFromMysql.control,
          },
          {
            id: 'Exposed',
            data: dataFromMysql.exposed,
          },
        ]);
      });
  }

  if (isLoading) return <Spinner />;

  if (isError) return <React.Fragment>Problem fetching data...</React.Fragment>;

  return (
    <React.Fragment>
      <Title>Weekly Total Results – {props.category}</Title>
      <div style={{ height: '165px', overflow: 'hidden' }}>
        <ResponsiveLine
          data={chartData}
          margin={{ top: 30, right: 20, bottom: 23, left: 65 }}
          xScale={{ format: '%Y-%m-%d', type: 'time', precision: 'day' }}
          xFormat='time:%Y-%m-%d'
          yScale={{ type: 'linear' }}
          axisBottom={{
            format: '%b %Y',
            tickRotation: 0,
            tickValues: 'every 1 months',
          }}
          axisLeft={{
            orient: 'left',
            format: '0.1s',
            tickRotation: 0,
            tickSize: 10,
            legend: 'Sales',
            legendOffset: -60,
            legendPosition: 'middle',
            tickValues: 3,
          }}
          colors={{ scheme: 'set2' }}
          lineWidth={1}
          enablePoints={true}
          pointSize={0}
          pointColor='white'
          pointBorderWidth={1}
          pointBorderColor={{ from: 'serieColor' }}
          enableArea={true}
          areaOpacity={0.2}
          pointLabel='y'
          pointLabelYOffset={-12}
          useMesh={true}
          enableGridX={true}
          gridYValues={3}
          gridXValues={4}
          motionStiffness={100}
          legends={[
            {
              anchor: 'top-right',
              direction: 'row',
              justify: false,
              translateX: 0,
              translateY: -30,
              itemsSpacing: 0,
              itemDirection: 'left-to-right',
              itemWidth: 80,
              itemHeight: 20,
              itemOpacity: 0.75,
              symbolSize: 12,
              symbolShape: 'circle',
              symbolBorderColor: 'rgba(0, 0, 0, .5)',
              effects: [
                {
                  on: 'hover',
                  style: {
                    itemBackground: 'rgba(0, 0, 0, .03)',
                    itemOpacity: 1,
                  },
                },
              ],
            },
          ]}
          defs={[
            // using helpers
            // will inherit colors from current element
            linearGradientDef('gradientA', [
              { offset: 0, color: 'inherit' },
              { offset: 100, color: 'inherit', opacity: 0 },
            ]),
            linearGradientDef('gradientB', [
              { offset: 0, color: '#000' },
              { offset: 100, color: 'inherit' },
            ]),
            // using plain object
            {
              id: 'gradientC',
              type: 'linearGradient',
              colors: [
                { offset: 0, color: '#faf047' },
                { offset: 100, color: '#e4b400' },
              ],
            },
          ]}
          // 2. defining rules to apply those gradients
          fill={[
            // match using object query
            { match: { id: 'react' }, id: 'gradientA' },
            // match using function
            { match: (d) => d.id === 'vue', id: 'gradientB' },
            // match all, will only affect 'elm', because once a rule match,
            // others are skipped, so now it acts as a fallback
            { match: '*', id: 'gradientC' },
          ]}
        />
      </div>
    </React.Fragment>
  );
};

export default MyResponsiveLine;
