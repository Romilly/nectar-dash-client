import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Chip from '@material-ui/core/Chip';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import axios from 'axios';
import { format } from 'd3-format';
import Spinner from './Spinner';

// add Material UI styling to create striped horizontal rows in table
const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

// add Material UI styling to headers in table
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
}))(TableCell);

const MyTable = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [tableData, setTableData] = useState([]);

  // Get top line values data from API
  const getTopData = async () => {
    try {
      const res = await axios.get(`/gettop/${props.metric}`);

      // if successful, push data to state
      setIsLoading(false);
      setTableData(res.data);
    } catch (err) {
      console.log(err);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading) {
    getTopData();
  }

  if (isLoading) return <Spinner />;

  if (isError) return <React.Fragment>Problem fetching data...</React.Fragment>;

  return (
    <React.Fragment>
      <Title>Top Line Values – {props.header}</Title>
      <Table size='small'>
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <StyledTableCell>Exposed</StyledTableCell>
            <StyledTableCell>Control</StyledTableCell>
            <StyledTableCell align='right'>Uplift</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tableData.map((row) => (
            <StyledTableRow key={row.product}>
              <TableCell>{row.product}</TableCell>
              <TableCell>
                {/* Format number with d3 for easier reading (adds k or M to large numbers)*/}
                {format('.4s')(row.exposed)}
              </TableCell>
              <TableCell>{format('.4s')(row.control)}</TableCell>
              <TableCell align='right'>
                {format('.3s')(row.uplift)}{' '}
                <Chip
                  size='small'
                  color='secondary'
                  variant='outlined'
                  label={`+` + format('.0%')(row.pct_uplift)}
                />
              </TableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
};

export default MyTable;
