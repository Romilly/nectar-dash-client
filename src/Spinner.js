import React from 'react';

const Spinner = () => {
  return (
    <div className='content-section'>
      <img
        src='/img/spinner2.gif'
        alt=''
        style={{ height: '40px', margin: '10px' }}
      />
    </div>
  );
};
export default Spinner;
